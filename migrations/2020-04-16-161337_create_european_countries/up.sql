CREATE TABLE european_countries (
  country VARCHAR PRIMARY KEY
);

INSERT INTO european_countries (country) VALUES ('Åland Islands');
INSERT INTO european_countries (country) VALUES ('Albania');
INSERT INTO european_countries (country) VALUES ('Andorra');
INSERT INTO european_countries (country) VALUES ('Austria');
INSERT INTO european_countries (country) VALUES ('Belarus');
INSERT INTO european_countries (country) VALUES ('Belgium');
INSERT INTO european_countries (country) VALUES ('Bosnia and Herzegovina');
INSERT INTO european_countries (country) VALUES ('Bulgaria');
INSERT INTO european_countries (country) VALUES ('Croatia');
INSERT INTO european_countries (country) VALUES ('Cyprus');
INSERT INTO european_countries (country) VALUES ('Czech Republic');
INSERT INTO european_countries (country) VALUES ('Denmark');
INSERT INTO european_countries (country) VALUES ('Estonia');
INSERT INTO european_countries (country) VALUES ('Faroe Islands');
INSERT INTO european_countries (country) VALUES ('Finland');
INSERT INTO european_countries (country) VALUES ('France');
INSERT INTO european_countries (country) VALUES ('Germany');
INSERT INTO european_countries (country) VALUES ('Gibraltar');
INSERT INTO european_countries (country) VALUES ('Greece');
INSERT INTO european_countries (country) VALUES ('Guernsey');
INSERT INTO european_countries (country) VALUES ('Holy See');
INSERT INTO european_countries (country) VALUES ('Hungary');
INSERT INTO european_countries (country) VALUES ('Iceland');
INSERT INTO european_countries (country) VALUES ('Ireland');
INSERT INTO european_countries (country) VALUES ('Isle of Man');
INSERT INTO european_countries (country) VALUES ('Italy');
INSERT INTO european_countries (country) VALUES ('Jersey');
INSERT INTO european_countries (country) VALUES ('Latvia');
INSERT INTO european_countries (country) VALUES ('Liechtenstein');
INSERT INTO european_countries (country) VALUES ('Lithuania');
INSERT INTO european_countries (country) VALUES ('Luxembourg');
INSERT INTO european_countries (country) VALUES ('Macedonia');
INSERT INTO european_countries (country) VALUES ('Malta');
INSERT INTO european_countries (country) VALUES ('Moldova');
INSERT INTO european_countries (country) VALUES ('Monaco');
INSERT INTO european_countries (country) VALUES ('Montenegro');
INSERT INTO european_countries (country) VALUES ('Netherlands');
INSERT INTO european_countries (country) VALUES ('Norway');
INSERT INTO european_countries (country) VALUES ('Poland');
INSERT INTO european_countries (country) VALUES ('Portugal');
INSERT INTO european_countries (country) VALUES ('Republic of Kosovo');
INSERT INTO european_countries (country) VALUES ('Romania');
INSERT INTO european_countries (country) VALUES ('Russian Federation');
INSERT INTO european_countries (country) VALUES ('San Marino');
INSERT INTO european_countries (country) VALUES ('Serbia');
INSERT INTO european_countries (country) VALUES ('Slovakia');
INSERT INTO european_countries (country) VALUES ('Slovenia');
INSERT INTO european_countries (country) VALUES ('Spain');
INSERT INTO european_countries (country) VALUES ('Svalbard and Jan Mayen');
INSERT INTO european_countries (country) VALUES ('Sweden');
INSERT INTO european_countries (country) VALUES ('Switzerland');
INSERT INTO european_countries (country) VALUES ('Ukraine');
INSERT INTO european_countries (country) VALUES ('UK');

CREATE OR REPLACE FUNCTION set_is_european() RETURNS trigger AS $$
BEGIN

IF(SELECT EXISTS (SELECT true FROM european_countries where country = NEW.country))
THEN NEW.is_european := true;
END IF;
RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER set_is_european_trigger BEFORE INSERT ON countries
FOR EACH ROW EXECUTE PROCEDURE set_is_european();
