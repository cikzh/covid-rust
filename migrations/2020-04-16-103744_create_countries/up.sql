CREATE TABLE countries (
  id SERIAL PRIMARY KEY,
  country VARCHAR,
  cases INTEGER,
  today_cases INTEGER,
  deaths INTEGER,
  today_deaths INTEGER,
  recovered INTEGER,
  active INTEGER,
  critical INTEGER,
  cases_per_one_million INTEGER,
  deaths_per_one_million INTEGER,
  is_european BOOLEAN NOT NULL DEFAULT false
)
