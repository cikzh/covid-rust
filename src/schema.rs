table! {
    countries (id) {
        id -> Int4,
        country -> Nullable<Varchar>,
        cases -> Nullable<Int4>,
        today_cases -> Nullable<Int4>,
        deaths -> Nullable<Int4>,
        today_deaths -> Nullable<Int4>,
        recovered -> Nullable<Int4>,
        active -> Nullable<Int4>,
        critical -> Nullable<Int4>,
        cases_per_one_million -> Nullable<Int4>,
        deaths_per_one_million -> Nullable<Int4>,
        is_european -> Bool,
    }
}

table! {
    european_countries (country) {
        country -> Varchar,
    }
}

allow_tables_to_appear_in_same_query!(
    countries,
    european_countries,
);
