#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate diesel;
#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

mod api;
mod models;
mod schema;

use rocket::http::Method;
use rocket_contrib::serve::StaticFiles;
use rocket_cors::{AllowedHeaders, AllowedOrigins};

#[database("covid")]
pub struct CovidDbConn(diesel::PgConnection);

fn cors() -> rocket_cors::Cors
{
    let allowed_origins = AllowedOrigins::some_exact(&["http://localhost:3000"]);
    rocket_cors::CorsOptions {
        allowed_origins,
        allowed_methods: vec![Method::Get, Method::Post, Method::Delete, Method::Options].into_iter().map(From::from).collect(),
        allowed_headers: AllowedHeaders::all(),
        allow_credentials: true,
        ..Default::default()
    }.to_cors().expect("Could not create CORS options")
}

fn main() {
    let mut rocket = rocket::ignite()
        .mount("/", StaticFiles::from("dist"))
        .attach(CovidDbConn::fairing())
        .attach(cors());

    rocket = api::mount(rocket);

    rocket.launch();
}
