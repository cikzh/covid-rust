use crate::CovidDbConn;

use crate::models::*;
use crate::diesel::prelude::*;
use crate::diesel::dsl::*;
    
use serde::{Serialize, Deserialize};
use rocket_contrib::json::Json;

#[derive(Serialize, Deserialize, Debug)]
pub struct ApiResponse
{
    error: bool,
    countries: Option<Vec<Country>>
}

pub fn mount(rocket: rocket::Rocket) -> rocket::Rocket
{
    rocket.mount("/api", routes![
        api,
        refetch,
    ])
}

#[get("/?<sort>&<filter>&<ascending>")]
pub fn api(conn: CovidDbConn, sort: Option<String>, filter: Option<String>, ascending: Option<bool>) -> Result<Json<ApiResponse>, diesel::result::Error>
{
    use crate::schema::countries::dsl::*;

    // NOTE (cikzh): We box the query, so we can dynamically build the query
    let mut query = countries.into_boxed();
    
    if sort.is_some()
    {
        // TODO (cikzh): Think of a more dynamic way to do this!
        let ordering: Box<dyn BoxableExpression<countries, diesel::pg::Pg, SqlType=()>> =
            if ascending.expect("ascending parameter not set!")
        {
                match sort.unwrap().as_str()
                {
                    "country" => Box::new(country.asc()),
                    "cases" => Box::new(cases.asc()),
                    "today_cases" => Box::new(today_cases.asc()),
                    "deaths" => Box::new(deaths.asc()),
                    "today_deaths" => Box::new(today_deaths.asc()),
                    "recovered" => Box::new(recovered.asc()),
                    "active" => Box::new(active.asc()),
                    "critical" => Box::new(critical.asc()),
                    "cases_per_one_million" => Box::new(cases_per_one_million.asc()),
                    "deaths_per_one_million" => Box::new(deaths_per_one_million.asc()),
                    _ => Box::new(country.asc())
                }
            }
        else
        {
            match sort.unwrap().as_str()
            {
                "country" => Box::new(country.desc()),
                "cases" => Box::new(cases.desc()),
                "today_cases" => Box::new(today_cases.desc()),
                "deaths" => Box::new(deaths.desc()),
                "today_deaths" => Box::new(today_deaths.desc()),
                "recovered" => Box::new(recovered.desc()),
                "active" => Box::new(active.desc()),
                "critical" => Box::new(critical.desc()),
                "cases_per_one_million" => Box::new(cases_per_one_million.desc()),
                "deaths_per_one_million" => Box::new(deaths_per_one_million.desc()),
                _ => Box::new(country.desc())
            }
        };

        query = query.order(ordering);
    }

    // NOTE (cikzh): Append a filter to the query if a filter is provided that is known to us
    // otherwise, just return the original query
    if filter.is_some()
    {
        query = match filter.unwrap().as_str()
        {
            "europe" => query.filter(is_european.eq(true)),
            _ => query
        }
    }

    // Execute the query
    let data = query.load::<Country>(&*conn)?;
    
    Ok(Json(
        ApiResponse {
            error: false,
            countries: Some(data)
        }
    ))
}

#[get("/refetch")]
pub fn refetch(conn: CovidDbConn) -> Result<String, diesel::result::Error>
{
    use crate::schema::countries::dsl::*;

    let data = get_data("https://coronavirus-19-api.herokuapp.com/countries")
        .expect("Error fetching data");

    // NOTE (cikzh): Delete the whole table
    diesel::delete(countries).execute(&*conn)?;

    let clean_data = clean_data(data);
    
    insert_into(countries)
        .values(&clean_data)
        .execute(&*conn)?;

    Ok("Refetch succesful".to_string())
}

#[inline]
fn get_data(url: &'static str) -> Result<Vec<NewCountry>, reqwest::Error>
{
    let response = reqwest::blocking::get(url)?
        .text()?;

    Ok(serde_json::from_str(&response).expect("Error parsing JSON"))
}

fn clean_data(input: Vec<NewCountry>) -> Vec<NewCountry>
{
    input
        .into_iter()
        .filter(|item| {
            item.country.as_ref() != Some(&"Total:".to_string())
                && item.country != None
                && item.country != Some("".to_string())
        })
        .collect()
}
