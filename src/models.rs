use serde::{Serialize, Deserialize};
use crate::schema::countries;

#[derive(Queryable, Serialize, Deserialize, Debug)]
pub struct Country
{
    pub id: i32,
    pub country: Option<String>,

    pub cases: Option<i32>,

    #[serde(alias = "todayCases")]
    pub today_cases: Option<i32>,

    pub deaths: Option<i32>,

    #[serde(alias = "todayDeaths")]
    pub today_deaths: Option<i32>,

    pub recovered: Option<i32>,
    pub active: Option<i32>,
    pub critical: Option<i32>,

    #[serde(alias = "casesPerOneMillion")]
    pub cases_per_one_million: Option<i32>,

    #[serde(alias = "deathsPerOneMillion")]
    pub deaths_per_one_million: Option<i32>,

    #[serde(alias = "isEuropean")]
    pub is_european: bool
}

#[derive(Insertable, Serialize, Deserialize, Debug)]
#[table_name = "countries"]
pub struct NewCountry
{
    pub country: Option<String>,

    pub cases: Option<i32>,

    #[serde(alias = "todayCases")]
    pub today_cases: Option<i32>,

    pub deaths: Option<i32>,

    #[serde(alias = "todayDeaths")]
    pub today_deaths: Option<i32>,

    pub recovered: Option<i32>,
    pub active: Option<i32>,
    pub critical: Option<i32>,

    #[serde(alias = "casesPerOneMillion")]
    pub cases_per_one_million: Option<i32>,

    #[serde(alias = "deathsPerOneMillion")]
    pub deaths_per_one_million: Option<i32>,
}
